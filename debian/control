Source: r-cran-patchwork
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Andreas Tille <tille@debian.org>
Section: gnu-r
Testsuite: autopkgtest-pkg-r
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-cran-ggplot2,
               r-cran-gtable,
               r-cran-rlang (>= 1.0.0),
               r-cran-cli,
               r-cran-farver
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-cran-patchwork
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-cran-patchwork.git
Homepage: https://cran.r-project.org/package=patchwork
Rules-Requires-Root: no

Package: r-cran-patchwork
Architecture: all
Depends: ${R:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: GNU R the composer of plots
 The 'ggplot2' package provides a strong API for sequentially
 building up a plot, but does not concern itself with composition of multiple
 plots. 'patchwork' is a package that expands the API to allow for
 arbitrarily complex composition of plots by, among others, providing
 mathematical operators for combining multiple plots. Other packages that try
 to address this need (but with a different approach) are 'gridExtra' and
 'cowplot'.
